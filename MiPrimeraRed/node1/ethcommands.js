console.log("Executing commands");

//Code to mine only if there are pending transactions
var state = '';
var mining_threads = 1
var min_balance = 100000000000000000000
var account = eth.coinbase
personal.unlockAccount(account, "password", 0);
miner.start();
function checkWork() {
     state = personal.listWallets[0].status;
    if (eth.getBlock("pending").transactions.length > 0) {
        if (eth.mining) return;
        console.log("## Pending transactions! Mining...");
        personal.unlockAccount(account, "password", 0);
        miner.start();
    }else {
        miner.stop();  // This param means nothing
        console.log("## No transactions! Mining stopped.");
    }
}
eth.filter("latest", function(err, block) { checkWork(); });
eth.filter("pending", function(err, block) { checkWork(); });
admin.startRPC("0.0.0.0", 8545, "*", "admin,personal,miner,web3,db,net,eth");


//Needed to generate the DAG and get some ETH's
//miner.start()
