geth --datadir /opt/ethereum/.ethereum_experiment --password /node/password account new | cut -f 2 -d ":" | tr -d " {}" | awk '$0="0x"$0' > accountKey
cp ./allocAccount/UTC* /opt/ethereum/.ethereum_experiment/keystore
geth --datadir /opt/ethereum/.ethereum_experiment  init /opt/ethereum/genesis.json
geth --datadir /opt/ethereum/.ethereum_experiment  --port 30303  --networkid 12 --exec "admin.nodeInfo.id" console | tr -d  '[[:space:]]' | tr -d '"'> pubKey
sed -i '1s/^/enode:\/\//' pubKey
echo "@$(ifconfig eth0 | grep "inet addr" | cut -d ':' -f 2 | cut -d ' ' -f 1):30303" >> pubKey
mv pubKey /opt/ethereum/.ethereum_experiment/
mv accountKey /common/accountKey
cp /opt/ethereum/.ethereum_experiment/nodekey /common/nodeKey
cp /opt/ethereum/.ethereum_experiment/pubKey /common/pubKey
chmod 777 /common/nodeKey
geth  --datadir /opt/ethereum/.ethereum_experiment  --port 30303  --networkid 12 js ./node/ethcommands.js
