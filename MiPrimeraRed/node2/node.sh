#! /bin/sh
while [ ! -f ./common/pubKey ]; do echo "Node1 isn't done generating its public key. Waiting..."; sleep 1; done
while [ ! -f ./common/accountKey ]; do echo "Node1 isn't done generating its admin account key. Waiting..."; sleep 1; done
cp ./allocAccount/UTC* /opt/ethereum/.ethereum_experiment/keystore
geth --datadir /opt/ethereum/.ethereum_experiment init /opt/ethereum/genesis.json
geth --datadir /opt/ethereum/.ethereum_experiment --port 30303 --networkid 12 --bootnodes $(cat ./common/pubKey) --etherbase "$(cat ./common/accountKey)" js ./node/ethcommands.js
