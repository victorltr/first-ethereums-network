console.log("Executing commands");

//Code to mine only if there are pending transactions
var mining_threads = 1
var min_balance = 30000000000000000000
var account = eth.coinbase
function checkWork() {
    if (eth.getBlock("pending").transactions.length > 0) {
        if (eth.mining) return;
        console.log("## Pending transactions! Mining...");
        miner.start(mining_threads);
    }else if (eth.getBalance(account) < min_balance){
        if(eth.mining) return;
        console.log("## Balance lower than 1 eth. Mining...");
        miner.start(mining_threads);
    }else {
        miner.stop(0);  // This param means nothing
        console.log("## No transactions! Mining stopped.");
    }
}
eth.filter("latest", function(err, block) { checkWork(); });
eth.filter("pending", function(err, block) { checkWork(); });
admin.startRPC("0.0.0.0", 8545, "*", "admin,personal,miner,web3,db,net,eth");


//Needed to generate the DAG and get some ETH's
miner.start()
